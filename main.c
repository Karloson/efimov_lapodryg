#include <iom16.h>
#include <intrinsics.h>
#include <comp_a90.h>


#define BUTTON_1 		0xF7FFF	
#define BUTTON_2 		0xEFFFF	
#define BUTTON_3 		0xDFFFF
#define BUTTON_4 		0xBFFFF	
#define BUTTON_5 		0x7FFFF	
#define BUTTON_6 	  0xFFBFF		
#define BUTTON_7 		0xFF7FF	
#define BUTTON_8 		0xFEFFF	
#define BUTTON_9 		0xFDFFF	
#define BUTTON_10 	0xFBFFF	
#define BUTTON_11		0xFFFDF	
#define BUTTON_12		0xFFFBF	
#define BUTTON_13		0xFFF7F	
#define BUTTON_14		0xFFEFF	
#define BUTTON_15 	0xFFDFF	
#define BUTTON_16		0xFFFFE	
#define BUTTON_17		0xFFFFD	
#define BUTTON_18		0xFFFFB	
#define BUTTON_19		0xFFFF7	
#define BUTTON_20		0xFFFEF	
#define BUTTON_0 		0xE7FFF

void init(void);               //������� �������������
void Button(void);						 //������� ���������� ������� ������ � ��������� ������� ����
void RELE(void);							 //������� ������������ ���� ����� ������� ������
void REGIST(void);

unsigned char flag = 0;
unsigned char string = 0;			 //����������� ������
unsigned char repit = 0;			 //������ ��� ���������� �������� ��������� (����� ���������� � �������� �������)
unsigned long button[3] = {0,0,0};
unsigned char numbers[10] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};
unsigned char number_out = 0;									//����� ������

void main( void )
{
  init();
  _SEI();
  while(1)
  {

  }
}

void init(void)
{
  DDRA = 0x7F;
  PORTA = 0x00;

  DDRB = 0x07;
  PORTB = 0xF8;

  DDRC = 0x7F;
  PORTC = 0x00;

  DDRD = 0xF0;
  PORTD = 0xF0;

  PORTD = 0xE0;

  PORTC = 0x00;
 	PORTA = numbers[0];
    
  TIMSK |= 1 << TOIE0;
  TCCR0 |= 1 << CS00 | 1 << CS01;                    //������������ 1 ������� �� 64

  for(unsigned char i=20; i>0; i--)
  {
  	PORTB &= ~(1<<2);
  	PORTB &= ~(1<<1);
  	PORTB |= (1<<1);
  	PORTB &= ~(1<<1);
  }
  PORTB |= (1<<0);
  PORTB &= ~(1<<0);
}

void Button(void)
{	
	unsigned char a = 0;
	unsigned char BIT_8 = 0;

	BIT_8 = (PINB & 0xF8) >> 3;
	button[repit] |= BIT_8;											//���������� ������ ������ � ������
	
	string+=1;
	if(string > 3)
	{
		string = 0;
		repit++;
	}	
	if(repit > 2)
	{
		repit = 0;
		if((button[0] == button[1]) && (button[1] == button[2]))
			RELE();
		button[0] = 0;
		button[1] = 0;
		button[2] = 0;
	}
	a = string + 4;
	PORTD &= 0x0F;
	PORTD |= 0xF0;
	PORTD &= ~(1 << a);											//����� ����� �� �����
	button[repit] = button[repit] << 5;
}


#pragma vector = TIMER0_OVF_vect                          //���������� �� �������� ������� �������� �� ������������
__interrupt void time (void)
{
  Button();                                                  //���������� ������� ������  
}


void RELE(void)
{
	switch(button[0])
 	{
 		case BUTTON_1:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[1];
 				number_out = 1;
 				REGIST();
 				break;
 			}
 		case BUTTON_2:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[2];
 				number_out = 2;
 				REGIST();
 				break;
 			}
 		case BUTTON_3:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[3];
 				number_out = 3;
 				REGIST();
 				break;
 			}
 		case BUTTON_4:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[4];
 				number_out = 4;
 				REGIST();
 				break;
 			}
 		case BUTTON_5:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[5];
 				number_out = 5;
 				REGIST();
 				break;
 			}
 		case BUTTON_6:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[6];
 				number_out = 6;
 				REGIST();
 				break;
 			}
 		case BUTTON_7:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[7];
 				number_out = 7;
 				REGIST();
 				break;
 			}
 		case BUTTON_8:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[8];
 				number_out = 8;
 				REGIST();
 				break;
 			}
 		case BUTTON_9:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[9];
 				number_out = 9;
 				REGIST();
 				break;
 			}
 		case BUTTON_10:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[0];
 				number_out = 10;
 				REGIST();
 				break;
 			}
 		case BUTTON_11:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[1];
 				number_out = 11;
 				REGIST();
 				break;
 			}
 		case BUTTON_12:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[2];
 				number_out = 12;
 				REGIST();
 				break;
 			}
 		case BUTTON_13:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[3];
 				number_out = 13;
 				REGIST();
 				break;
 			}
 		case BUTTON_14:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[4];
 				number_out = 14;
 				REGIST();
 				break;
 			}
 		case BUTTON_15:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[5];
 				number_out = 15;
 				REGIST();
 				break;
 			}
 		case BUTTON_16:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[6];
 				number_out = 16;
 				REGIST();
 				break;
 			}
 		case BUTTON_17:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[7];
 				number_out = 17;
 				REGIST();
 				break;
 			}
 		case BUTTON_18:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[8];
 				number_out = 18;
 				REGIST();
 				break;
 			}
 		case BUTTON_19:
 			{
 				PORTC = numbers[1];
 				PORTA = numbers[9];
 				number_out = 19;
 				REGIST();
 				break;
 			}
 		case BUTTON_20:
 			{
 				PORTC = numbers[2];
 				PORTA = numbers[0];
 				number_out = 20;
 				REGIST();
 				break;
 			}
 		case BUTTON_0:
 			{
 				PORTC = 0x00;
 				PORTA = numbers[0];
 				number_out = 0;
 				REGIST();
 				break;
 			}
 	}	
}


void REGIST(void)
{
	for(unsigned char i = 20; i > number_out; i--)
 	{
 		PORTB &= ~(1<<2);
 		PORTB &= ~(1<<1);
  	PORTB |= (1<<1);
  	PORTB &= ~(1<<1);
 	}

 	PORTB |= (1<<2);
	PORTB &= ~(1<<1);
  PORTB |= (1<<1);
  PORTB &= ~(1<<1); 	

 	for(unsigned char i=number_out; i>1; i--)
 	{
 		PORTB &= ~(1<<2);
 		PORTB &= ~(1<<1);
  	PORTB |= (1<<1);
  	PORTB &= ~(1<<1);
 	}
 	PORTB |= (1<<0);
  PORTB &= ~(1<<0);
}